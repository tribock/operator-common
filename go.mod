module gitlab.com/tribock/operator-common

require (
	github.com/caos/operator-aerospike v1.0.1
	github.com/ghodss/yaml v1.0.0
	github.com/yabslabs/utils v0.0.0-20190318101319-f105461806c1
	google.golang.org/grpc v1.19.0
	k8s.io/api v0.0.0-20181221193117-173ce66c1e39
	k8s.io/apiextensions-apiserver v0.0.0-20190314165226-5c9e5bbeace1
	k8s.io/apimachinery v0.0.0-20190126155707-0e6dcdd1b5ce
	k8s.io/client-go v10.0.0+incompatible
	k8s.io/code-generator v0.0.0-20181206115026-3a2206dd6a78
	sigs.k8s.io/controller-runtime v0.1.10
	sigs.k8s.io/controller-tools v0.1.9
)
