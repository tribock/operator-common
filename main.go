/*
Copyright 2018 tribock.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	v1 "github.com/caos/operator-aerospike/pkg/apis/aerospike/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// TODO: create array of observable objects
var k8sObjects = []runtime.Object{&appsv1.Deployment{}, &corev1.Service{}, &v1beta1.Ingress{}, &corev1.Secret{}, &v1.Aerospike{}}

func main() {

	// logf.SetLogger(logf.ZapLogger(false))
	// log := logf.Log.WithName("initialize operator")

	// // Get a config to talk to the apiserver
	// log.V(4).Info("setting up client for manager")
	// cfg, err := config.GetConfig()
	// if err != nil {

	// 	log.Error(err, "unable to set up client config")
	// 	os.Exit(1)
	// }

	// // Create a new Cmd to provide shared dependencies and start components
	// log.V(4).Info("setting up manager")
	// mgr, err := manager.New(cfg, manager.Options{})
	// if err != nil {
	// 	log.Error(err, "unable to set up overall controller manager")
	// 	os.Exit(1)
	// }

	// log.V(4).Info("Registering Components.")

	// // Setup Scheme for all resources
	// log.V(4).Info("setting up scheme")
	// if err := apis.AddToScheme(mgr.GetScheme()); err != nil {
	// 	log.Error(err, "unable add APIs to scheme")
	// 	os.Exit(1)
	// }

	// // Setup all Controllers
	// log.V(4).Info("Setting up controller")

	// controller.CreateControllers(mgr, k8sObjects)

	// log.V(4).Info("setting up webhooks")
	// if err := webhook.AddToManager(mgr); err != nil {
	// 	log.Error(err, "unable to register webhooks to the manager")
	// 	os.Exit(1)
	// }

	// // Start the Cmd
	// log.V(4).Info("Starting the Cmd.")
	// if err := mgr.Start(signals.SetupSignalHandler()); err != nil {
	// 	log.Error(err, "unable to run the manager")
	// 	os.Exit(1)
	// }
}
