# boilerplate to bootstrap service operator

## replacement

to use the boilerplate properly you need to replace the word boilerplate in the whole project with your service name

### strings to replace

Pay attention to replace with up/lowercase sensitivity enabled!

- boilerplate
- Boilerplate

### files to exlude

add the following string to the excluded file:

`vendor, tools.go, go.sum, go.mod` 


## regenerate modules

```bash
go mod init
# add required dep manually
go mod edit -require github.com/caos/operator-aerospike@v0.0.0-20190130091944-61de0ce3129d



go mod edit -require k8s.io/api@v0.0.0-20181221193117-173ce66c1e39
go mod edit -require sigs.k8s.io/controller-tools@v0.1.8
go mod edit -require sigs.k8s.io/controller-runtime@v0.1.9


go mod edit -require k8s.io/api@v0.0.0-20181221193117-173ce66c1e39
go mod edit -require k8s.io/apimachinery@v0.0.0-20190126155707-0e6dcdd1b5ce
go mod edit -require k8s.io/client-go@v10.0.0+incompatible
go mod edit -require k8s.io/code-generator@v0.0.0-20181206115026-3a2206dd6a78
go mod edit -require sigs.k8s.io/controller-runtime@v0.1.10
go mod edit -require sigs.k8s.io/controller-tools@v0.1.9

```

## makefile

The makefile contains the steps to install and run the application/operator.
Before executing the makefile make sure you have a minikube cluster up and running e.g.:

```bash
minikube start --vm-driver=vmwarefusion
``` 

The makefile configures your local kubeconfig to connect to the minikube cluster.
If you want to test against another cluster change the context switch in the makefile:

`kubectl config use-context minikube` --> `kubectl config use-context my-custom-context`
