/*
Copyright 2018 tribock.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"time"

	apiextv1beta1 "k8s.io/apiextensions-apiserver/pkg/apis/apiextensions/v1beta1"
	apiextcs "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	"github.com/yabslabs/utils/logging"
)

type CRD struct {
	TypeMeta    meta_v1.TypeMeta
	CRDPlural   string
	CRDGroup    string
	CRDVersion  string
	FullCRDName string
	ShortNames  []string
}

// Create the CRD resource, ignore error if it already exists
func CreateCRD(clientset apiextcs.Interface, crd *CRD) error {
	newCrd := &apiextv1beta1.CustomResourceDefinition{
		ObjectMeta: meta_v1.ObjectMeta{Name: crd.FullCRDName},
		Spec: apiextv1beta1.CustomResourceDefinitionSpec{
			Group:   crd.CRDGroup,
			Version: crd.CRDVersion,
			Scope:   apiextv1beta1.NamespaceScoped,
			Names: apiextv1beta1.CustomResourceDefinitionNames{
				Plural:     crd.CRDPlural,
				Kind:       crd.TypeMeta.Kind,
				ShortNames: crd.ShortNames,
			},
		},
	}
	_, err := clientset.ApiextensionsV1beta1().CustomResourceDefinitions().Create(newCrd)
	if apierrors.IsAlreadyExists(err) {
		logging.WithID("OPERATOR-COMMON-CRD-000").Debug("crd already present in cluster")
		return nil
	} else if err != nil {
		logging.WithID("OPERATOR-COMMON-CRD-001").Debug(err)
		return err
	}

	// Wait for the CRD to be created before we use it (only needed if its a new one)
	time.Sleep(3 * time.Second)
	return err

	// Note the original apiextensions example adds logic to wait for creation and exception handling
}

func AddKnownTypes(scheme *runtime.Scheme, objects ...K8sObjectWrapper) error {
	return addKnownTypes(scheme, objects...)
}

func addKnownTypes(scheme *runtime.Scheme, objects ...K8sObjectWrapper) error {
	arr := make([]runtime.Object, len(objects))
	for i, obj := range objects {
		arr[i] = obj
	}
	scheme.AddKnownTypes(SchemeGroupVersion, arr...)
	meta_v1.AddToGroupVersion(scheme, SchemeGroupVersion)
	return nil
}
