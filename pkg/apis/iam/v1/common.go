package v1

import (
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

// K8sObjectWrapper that implements ObjectAreEqual Method to compare k8s objects
type K8sObjectWrapper interface {
	runtime.Object
	ObjectsAreEqual(y interface{}) bool
	GetK8sObj() runtime.Object
	SetK8sObj(runtime.Object)
	SetMinorVersion() error
}

type K8sMetaObject interface {
	K8sObjectWrapper
	ManageComponents(reconcile.Reconciler, reconcile.Request, K8sObjectWrapper) (reconcile.Result, error)
}
