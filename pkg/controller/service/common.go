package service

type EnvVar struct {
	Name  string
	Value string
}

type CommonProps struct {
	Name         string
	Namespace    string
	Version      string
	MajorVersion string
}
