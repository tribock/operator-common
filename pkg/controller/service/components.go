package service

import (
	"context"
	"log"

	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"
	"k8s.io/apimachinery/pkg/api/errors"
	apiv1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"

	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

func (r *Reconcile) GetFoundObject(kind string) v1.K8sObjectWrapper {
	switch kind {
	case "Deployment":
		return &DeploymentWrapper{Deployment: &appsv1.Deployment{}}
	case "Service":
		return &ServiceWrapper{Service: &corev1.Service{}}
	case "Ingress":
		return &IngressWrapper{Ingress: &v1beta1.Ingress{}}
	// case "Secret":
	// 	return &SecretWrapper{Secret: &corev1.Secret{}}
	case "Aerospike":
		return &AerospikeWrapper{Aerospike: &v1.Aerospike{}}
	default:
		log.Fatal("no type recognized for string ", kind)

	}
	return nil

}

func (r *Reconcile) DeployRuntimeObject(request reconcile.Request, instance v1.K8sMetaObject, obj v1.K8sObjectWrapper) (reconcile.Result, error) {
	metav1Object := obj.(metav1.Object)
	if err := controllerutil.SetControllerReference(instance.(apiv1.Object), metav1Object.(apiv1.Object), r.scheme); err != nil {
		return reconcile.Result{}, err
	}

	kind := obj.GetObjectKind().GroupVersionKind().Kind
	log.Println(kind)
	found := r.GetFoundObject(kind)

	err := r.Get(context.TODO(), types.NamespacedName{Name: metav1Object.GetName(), Namespace: metav1Object.GetNamespace()}, found.GetK8sObj())

	if err != nil && errors.IsNotFound(err) {
		log.Printf("Creating %s %s/%s\n", kind, metav1Object.GetName(), metav1Object.GetNamespace())
		err = r.Create(context.TODO(), obj.GetK8sObj())
		if err != nil {
			return reconcile.Result{}, err
		}
	} else if err != nil {
		return reconcile.Result{}, err
	}

	if !obj.ObjectsAreEqual(found) {
		log.Printf("Updating %s %s/%s\n", obj.GetObjectKind().GroupVersionKind().Kind, metav1Object.GetName(), metav1Object.GetNamespace())
		err = r.Update(context.TODO(), obj.GetK8sObj())
		if err != nil {
			return reconcile.Result{}, err
		}
	}
	return reconcile.Result{}, nil
}
