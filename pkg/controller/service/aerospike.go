package service

import (
	"reflect"
	"strings"

	"k8s.io/client-go/rest"

	"k8s.io/apimachinery/pkg/runtime"

	"github.com/yabslabs/utils/logging"
	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"

	apiextcs "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type AerospikeInstance interface {
	GetAsSpec() *AsSpec
	SetMinorVersion() error
}

type AsSpec struct {
	v1.AerospikeSpec
	CommonProps
}

func (r *Reconcile) CreateAerospike(inst interface{}) *AerospikeWrapper {

	instance := inst.(AerospikeInstance)
	spec := instance.GetAsSpec()
	aerospike := &AerospikeWrapper{
		Aerospike: &v1.Aerospike{
			TypeMeta: metav1.TypeMeta{
				Kind: "Aerospike",
			},

			ObjectMeta: metav1.ObjectMeta{
				Name:      spec.Name + "-" + spec.MajorVersion,
				Namespace: spec.Namespace,
			},
			Spec: v1.AerospikeSpec{
				ServiceName: spec.Name,
				Replicas:    spec.AerospikeSpec.Replicas,
				Aeroconfig:  spec.AerospikeSpec.Aeroconfig,
				MemoryLimit: spec.AerospikeSpec.MemoryLimit,
				CPULimit:    spec.AerospikeSpec.CPULimit,
				StorageSize: spec.AerospikeSpec.StorageSize,
			},
		},
	}

	return aerospike
}

// AerospikeWrapper to abstract v1beta1.Aerospike
type AerospikeWrapper struct {
	*v1.Aerospike
	AsSpec
	AerospikeInstance
}

// GetK8sObj implementation for K8sObjectWrapper interface
func (i *AerospikeWrapper) GetK8sObj() runtime.Object {
	return i.Aerospike

}

// SetK8sObj implementation for K8sObjectWrapper interface
func (i *AerospikeWrapper) SetK8sObj(obj runtime.Object) {
	i.Aerospike = obj.(*v1.Aerospike)
}

func GetAerospikeWrapper() v1.K8sObjectWrapper {
	return &AerospikeWrapper{&v1.Aerospike{}, AsSpec{}, nil}
}

// SetK8sObj implementation for K8sObjectWrapper interface
func (i *AerospikeWrapper) RegisterCRD(config *rest.Config) error {
	crd := v1.CRD{
		TypeMeta:   i.TypeMeta,
		CRDPlural:  "aerospikes",
		CRDGroup:   "iam.rootd.ch",
		CRDVersion: "v1",
	}
	// create clientset and create our CRD, this only need to run once
	clientset, err := apiextcs.NewForConfig(config)
	if err != nil {
		logging.WithID("AS-CRDWATCHER-kjehfi9u").Fatal(err)
	}
	return v1.CreateCRD(clientset, &crd)

}

func (i *AerospikeWrapper) SetMinorVersion() error {
	spec := i.GetSpec()
	// get majjor version and save it to instance object
	spec.MajorVersion = strings.Split(spec.Version, ".")[0]
	return nil
}

func (i *AerospikeWrapper) GetSpec() *AsSpec {
	return &i.AsSpec

}

// ObjectsAreEqual implementation for K8sObjectWrapper interface, uses reflect.DeepEqual to compare Objects
func (i *AerospikeWrapper) ObjectsAreEqual(oldie interface{}) bool {
	old, ok := oldie.(*AerospikeWrapper)
	if !ok {
		return false
	}
	current := old.DeepCopyObject().(*v1.Aerospike)
	old.SetName(i.GetName())
	old.SetNamespace(i.GetNamespace())
	old.Spec = i.Spec
	return reflect.DeepEqual(*current, *old.Aerospike)
}
