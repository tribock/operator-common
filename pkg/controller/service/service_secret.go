package service

// import (
// 	"fmt"
// 	"reflect"

// 	"encoding/base64"

// 	corev1 "k8s.io/api/core/v1"
// 	"k8s.io/apimachinery/pkg/runtime"

// 	configuration "gitlab.com/tribock/operator-common/config"
// 	iamv1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"
// 	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
// 	"sigs.k8s.io/controller-runtime/pkg/reconcile"
// )
// type ServiceInstance interface {
// 	DeployInstance
// 	GetServiceSpec() *ServiceSpec
// }

// type ServiceSpec struct {
// 	Spec
// 	ServicePath     string
// 	BaseURL         string
// 	GRPC            bool
// 	GRPCServicePath string
// }

// func (r *Reconcile) createSecret(request reconcile.Request, instance ServiceInstance, config *configuration.Config) *SecretWrapper {
// 	spec := instance.GetIngressSpec()
// 	decodedSecret, err := base64.StdEncoding.DecodeString(instance.Spec.GoogleCloudPermission)
// 	if err != nil {
// 		fmt.Println("not able to decode string - error:", err)
// 		return &SecretWrapper{}
// 	}
// 	secret := &SecretWrapper{
// 		Secret: &corev1.Secret{
// 			TypeMeta: metav1.TypeMeta{
// 				Kind: "Secret",
// 			},
// 			ObjectMeta: metav1.ObjectMeta{
// 				Name:      instance.Name + "-" + instance.Spec.MajorVersion,
// 				Namespace: instance.GetNamespace(),
// 			},
// 			Data: map[string][]byte{"key.json": decodedSecret},
// 		},
// 	}

// 	return secret
// }

// // SecretWrapper to abstract v1beta1.Secret
// type SecretWrapper struct {
// 	*corev1.Secret
// }

// // GetK8sObj implementation for K8sObjectWrapper interface
// func (i *SecretWrapper) GetK8sObj() runtime.Object {
// 	return i.Secret
// }

// // SetK8sObj implementation for K8sObjectWrapper interface
// func (i *SecretWrapper) SetK8sObj(obj runtime.Object) {
// 	i.Secret = obj.(*corev1.Secret)
// }

// // ObjectsAreEqual implementation for K8sObjectWrapper interface, uses reflect.DeepEqual to compare Objects
// func (i *SecretWrapper) ObjectsAreEqual(oldie interface{}) bool {
// 	old, ok := oldie.(*SecretWrapper)
// 	if !ok {
// 		return false
// 	}
// 	current := old.DeepCopyObject().(*corev1.Secret)
// 	old.SetName(i.GetName())
// 	old.SetNamespace(i.GetNamespace())
// 	old.Data = i.Data
// 	return reflect.DeepEqual(*current, *old.Secret)
// }
