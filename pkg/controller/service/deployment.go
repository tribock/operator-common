package service

import (
	"reflect"
	"strconv"
	"strings"

	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type DeployInstance interface {
	GetSpec() *DeploySpec
	SetMinorVersion() error
}

type DeploySpec struct {
	ReplicaCount string
	Env          []EnvVar
	MajorVersion string
	Namespace    string
	Name         string
	Version      string
	PullSecret   string
	Image        string
}

func (r *Reconcile) CreateDeployment(request reconcile.Request, inst interface{}) *DeploymentWrapper {
	instance := inst.(DeployInstance)
	containerEnv := []corev1.EnvVar{}
	spec := instance.GetSpec()
	replicas, err := strconv.Atoi(spec.ReplicaCount)
	if err != nil {

		replicas = 1
	}
	for _, envVar := range spec.Env {
		containerEnv = append(containerEnv, corev1.EnvVar{
			Name:  envVar.Name,
			Value: envVar.Value,
		})
	}

	deploy := &DeploymentWrapper{

		&appsv1.Deployment{
			TypeMeta: metav1.TypeMeta{
				Kind: "Deployment",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      spec.Name + "-" + spec.MajorVersion,
				Namespace: spec.Namespace,
			},
			Spec: appsv1.DeploymentSpec{
				Replicas: getInt32(replicas),
				Selector: &metav1.LabelSelector{
					MatchLabels: map[string]string{"app": spec.Name + "-" + spec.MajorVersion},
				},
				Template: corev1.PodTemplateSpec{

					ObjectMeta: metav1.ObjectMeta{Labels: map[string]string{"app": spec.Name + "-" + spec.MajorVersion}},
					Spec: corev1.PodSpec{

						Containers: []corev1.Container{
							{
								Name:  spec.Name,
								Image: spec.Image,
								Env:   containerEnv,
							},
						},
						ImagePullSecrets: []corev1.LocalObjectReference{
							{
								Name: spec.PullSecret,
							},
						},
					},
				},
			},
		},
		*spec,
	}
	return deploy
}

func getInt32(i int) *int32 {
	retVal := int32(i)
	return &retVal
}

// DeploymentWrapper to abstract appsv1.Deployment
type DeploymentWrapper struct {
	*appsv1.Deployment
	DeploySpec
}

func GetDeploymentWrapper() v1.K8sObjectWrapper {
	return &DeploymentWrapper{&appsv1.Deployment{}, DeploySpec{}}
}

// GetK8sObj implementation for K8sObjectWrapper interface
func (d *DeploymentWrapper) GetK8sObj() runtime.Object {
	return d.Deployment
}

// SetK8sObj implementation for K8sObjectWrapper interface
func (d *DeploymentWrapper) SetK8sObj(obj runtime.Object) {
	d.Deployment = obj.(*appsv1.Deployment)
}

// ObjectsAreEqual implementation for K8sObjectWrapper interface, uses reflect.DeepEqual to compare Objects
func (d *DeploymentWrapper) ObjectsAreEqual(oldie interface{}) bool {
	old := oldie.(*DeploymentWrapper)
	current := old.DeepCopyObject().(*appsv1.Deployment)
	old.SetName(d.GetName())
	old.SetNamespace(d.GetNamespace())
	if len(old.Spec.Template.Spec.Containers) < 1 {
		old.Spec.Template.Spec.Containers = d.Spec.Template.Spec.Containers
	} else {
		old.Spec.Template.Spec.Containers[0].Env = d.Spec.Template.Spec.Containers[0].Env
		old.Spec.Template.Spec.Containers[0].Image = d.Spec.Template.Spec.Containers[0].Image
	}
	old.Spec.Replicas = d.Spec.Replicas
	old.Spec.Selector = d.Spec.Selector
	return reflect.DeepEqual(*current, *old.Deployment)
}

func (i *DeploymentWrapper) SetMinorVersion() error {
	spec := i.GetSpec()
	// get majjor version and save it to instance object
	spec.MajorVersion = strings.Split(spec.Version, ".")[0]
	return nil
}

func (i *DeploymentWrapper) GetSpec() *DeploySpec {
	return &i.DeploySpec
}
