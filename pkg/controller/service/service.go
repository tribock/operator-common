package service

import (
	"reflect"
	"strings"

	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"

	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type ServiceInstance interface {
	DeployInstance
	GetServiceSpec() *ServiceSpec
}

func (i *ServiceWrapper) SetMinorVersion() error {
	spec := i.GetSpec()
	// get majjor version and save it to instance object
	spec.MajorVersion = strings.Split(spec.Version, ".")[0]
	return nil
}

type ServiceSpec struct {
	CommonProps
	ServicePath     string
	BaseURL         string
	GRPC            bool
	GRPCServicePath string
	HttpsPort       int32
}

func (r *Reconcile) CreateService(request reconcile.Request, inst interface{}) *ServiceWrapper {
	instance := inst.(ServiceInstance)
	spec := instance.GetServiceSpec()
	service := &ServiceWrapper{
		Service: &corev1.Service{
			TypeMeta: metav1.TypeMeta{
				Kind: "Service",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      spec.Name + "-" + spec.MajorVersion,
				Namespace: spec.Namespace,
				Labels: map[string]string{
					"app": spec.Name + "-" + spec.MajorVersion,
				},
			},
			Spec: corev1.ServiceSpec{
				Ports: []corev1.ServicePort{
					{
						Name: "https",
						Port: spec.HttpsPort,
					},
				},
				Type: "LoadBalancer",
				Selector: map[string]string{
					"app": spec.Name + "-" + spec.MajorVersion,
				},
			},
		},
	}
	if spec.GRPC {
		service.Spec.Ports = append(service.Spec.Ports, corev1.ServicePort{
			Name: "grpc",
			Port: 8080,
		})
	}
	return service
}

// ServiceWrapper to abstract corev1.Service
type ServiceWrapper struct {
	*corev1.Service
	ServiceInstance
}

// GetK8sObj implementation for K8sObjectWrapper interface
func (s *ServiceWrapper) GetK8sObj() runtime.Object {
	return s.Service
}

// SetK8sObj implementation for K8sObjectWrapper interface
func (s *ServiceWrapper) SetK8sObj(obj runtime.Object) {
	s.Service = obj.(*corev1.Service)
}

func GetServiceWrapper() v1.K8sObjectWrapper {
	return &ServiceWrapper{&corev1.Service{}, nil}
}

// ObjectsAreEqual implementation for K8sObjectWrapper interface, uses reflect.DeepEqual to compare Objects
func (s *ServiceWrapper) ObjectsAreEqual(oldie interface{}) bool {
	old, ok := oldie.(*ServiceWrapper)
	if !ok {
		return false
	}
	found := old.DeepCopyObject().(*corev1.Service)
	old.SetName(s.GetName())
	old.SetNamespace(s.GetNamespace())
	old.Spec.Ports = s.Spec.Ports
	s.Spec.ClusterIP = old.Spec.ClusterIP

	// set ResourceVersion for Update
	s.SetResourceVersion(old.GetResourceVersion())

	return reflect.DeepEqual(*found, *old.Service)
}
