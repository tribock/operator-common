package service

import (
	"reflect"
	"strings"

	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"
	v1beta1 "k8s.io/api/extensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	intstr "k8s.io/apimachinery/pkg/util/intstr"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
)

type IngressInstance interface {
	GetSpec() *IngressSpec
	SetMajorVersion() error
}

type IngressSpec struct {
	CommonProps
	IngressPath     string
	BaseURL         string
	GRPC            bool
	GRPCIngressPath string
}

func (r *Reconcile) CreateIngress(request reconcile.Request, inst interface{}) *IngressWrapper {
	instance := inst.(IngressInstance)

	spec := instance.GetSpec()
	ingress := &IngressWrapper{
		Ingress: &v1beta1.Ingress{
			TypeMeta: metav1.TypeMeta{
				Kind: "Ingress",
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:      spec.Name + "-" + spec.MajorVersion,
				Namespace: spec.Namespace,
			},
			Spec: v1beta1.IngressSpec{
				Rules: []v1beta1.IngressRule{
					{
						Host: spec.BaseURL,
						IngressRuleValue: v1beta1.IngressRuleValue{
							HTTP: &v1beta1.HTTPIngressRuleValue{
								Paths: []v1beta1.HTTPIngressPath{
									{
										Path: spec.IngressPath,
										Backend: v1beta1.IngressBackend{
											ServiceName: spec.Name + "-" + spec.MajorVersion,
											ServicePort: intstr.Parse("80"),
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	if spec.GRPC {
		ingress.Spec.Rules[0].HTTP.Paths = append(ingress.Spec.Rules[0].HTTP.Paths,
			v1beta1.HTTPIngressPath{
				Path: spec.GRPCIngressPath,
				Backend: v1beta1.IngressBackend{
					ServiceName: spec.Name + "-" + spec.MajorVersion,
					ServicePort: intstr.Parse("8080"),
				},
			},
		)
	}
	return ingress
}

// IngressWrapper to abstract v1beta1.Ingress
type IngressWrapper struct {
	*v1beta1.Ingress
	IngressSpec
}

func GetIngressWrapper() v1.K8sObjectWrapper {
	return &IngressWrapper{&v1beta1.Ingress{}, IngressSpec{}}
}

// GetK8sObj implementation for K8sObjectWrapper interface
func (i *IngressWrapper) GetK8sObj() runtime.Object {
	return i.Ingress
}

// SetK8sObj implementation for K8sObjectWrapper interface
func (i *IngressWrapper) SetK8sObj(obj runtime.Object) {
	i.Ingress = obj.(*v1beta1.Ingress)
}
func (i *IngressWrapper) SetMinorVersion() error {
	spec := i.GetSpec()
	// get majjor version and save it to instance object
	spec.MajorVersion = strings.Split(spec.Version, ".")[0]
	return nil
}

func (i *IngressWrapper) GetSpec() *IngressSpec {
	return &i.IngressSpec
}

// ObjectsAreEqual implementation for K8sObjectWrapper interface, uses reflect.DeepEqual to compare Objects
func (i *IngressWrapper) ObjectsAreEqual(oldie interface{}) bool {
	old, ok := oldie.(*IngressWrapper)
	if !ok {
		return false
	}
	current := old.DeepCopyObject().(*v1beta1.Ingress)
	old.SetName(i.GetName())
	old.SetNamespace(i.GetNamespace())
	old.Spec = i.Spec
	return reflect.DeepEqual(*current, *old.Ingress)
}
