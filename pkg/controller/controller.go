/*
Copyright 2018 tribock.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"log"
	"os"

	v1 "gitlab.com/tribock/operator-common/pkg/apis/iam/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/manager"
)

// AddToManagerFuncs is a list of functions to add all Controllers to the Manager
var AddToManagerFuncs []func(manager.Manager, runtime.Object, string) error

// AddToManager adds all Controllers to the Manager
func AddToManager(m manager.Manager, kind runtime.Object, connectorName string) error {
	for _, f := range AddToManagerFuncs {

		if err := f(m, kind, connectorName); err != nil {
			return err
		}
	}
	return nil
}

func CreateControllers(mgr manager.Manager, objs []v1.K8sObjectWrapper, connectorName string) {
	for _, obj := range objs {
		if err := AddToManager(mgr, obj.GetK8sObj(), connectorName); err != nil {
			log.Println(err, "unable to register controllers to the manager")
			os.Exit(1)
		}
	}

}
