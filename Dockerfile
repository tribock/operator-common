FROM alpine:3.6
RUN apk add --no-cache ca-certificates
RUN mkdir /app
ADD /target/main /app/
ADD config/* /app/config/
ADD . tmp/
RUN cp -r tmp/services/ /app/ 2>/dev/null || : && \
    cp -r tmp/templates/ /app/ 2>/dev/null || : && \
    cp -r tmp/resources/ /app/ 2>/dev/null || : && \
    cp -r tmp/storage/ /app/ 2>/dev/null || : && \
    rm -rf tmp

WORKDIR /app
CMD ["/app/main"]

